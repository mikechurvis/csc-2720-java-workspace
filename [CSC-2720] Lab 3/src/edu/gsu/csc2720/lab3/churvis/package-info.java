/**
 * This package implements all specifications for Lab 3, including a tester
 * class which runs a console app that invokes test methods which verify the
 * correctness of our implementation.
 * @author MikeChurvis
 */
package edu.gsu.csc2720.lab3.churvis;
