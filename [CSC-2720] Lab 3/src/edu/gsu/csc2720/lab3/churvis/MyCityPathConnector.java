package edu.gsu.csc2720.lab3.churvis;

import edu.gsu.csc2720.lab3.City;
import edu.gsu.csc2720.lab3.CityPath;
import edu.gsu.csc2720.lab3.CityPathConnector;
import edu.gsu.csc2720.lab3.CitySelector;

/**
 * Determines CityPaths from a source City to a destination
 * City.
 * @author MikeChurvis
 */
public class MyCityPathConnector implements CityPathConnector {

	/**
	 * Determines a CityPath from {@code source} to {@code dest},
	 * using a stack internally.
	 *
	 * @param selector The selector that determines the order of city travel.
	 * @param source The source city.
	 * @param dest The destination city.
	 * @return The CityPath from {@code source} to {@code dest}.
	 */
	public CityPath getPath(final CitySelector selector,
			final City source,
			final City dest) {
		return null;
	}

}
