package edu.gsu.csc2720.lab2.churvis;

//import edu.gsu.csc2720.lab2.Polynomial;
import edu.gsu.csc2720.lab2.PolynomialTester;

/**
 * A console application that invokes a battery of tests on the TreePolynomial
 * data structure.
 * @author Michael
 *
 */
public final class TreePolynomialTester {

	/**
	 * Privatized default constructor prevents instantiation.
	 */
	private TreePolynomialTester() {
		// Does nothing.
	}

	/**
	 * The console app's main method.
	 * @param args Not used here.
	 */
	public static void main(final String[] args) {
		PolynomialTester.runTests(new TreePolynomialFactory());

		//TreePolynomialFactory tpf = new TreePolynomialFactory();
		//Polynomial tp = tpf.createPolynomial();

		//System.out.println(tp.toString());
	}
}
