package edu.gsu.csc2720.lab2.churvis;

import edu.gsu.csc2720.lab2.Polynomial;
import edu.gsu.csc2720.lab2.PolynomialFactory;

/**
 * Creates TreePolynomial instances.
 * @author Michael
 *
 */
public class TreePolynomialFactory implements PolynomialFactory {

	/**
	 * Constructs a new, empty TreePolynomial.
	 * @return A new, empty TreePolynomial.
	 */
	public Polynomial createPolynomial() {
		return new TreePolynomial();
	}
}
