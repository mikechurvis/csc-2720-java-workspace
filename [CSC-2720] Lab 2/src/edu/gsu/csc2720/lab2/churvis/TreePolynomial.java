package edu.gsu.csc2720.lab2.churvis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import edu.gsu.csc2720.lab2.Polynomial;

/**
 * A TreeMap-backed implementation of the Polynomial interface defined by
 * E. A. Reddick. This implementation may handle negative exponents (powers
 * greater than one in the denominator).
 * @author MikeChurvis
 */
public final class TreePolynomial implements Polynomial {

	/**
	 * The internal data structure for storage of the coefficients of this
	 * Polynomial. Maps a coefficient value to a power key.
	 */
	private TreeMap<Integer, Integer> coeffToPowerMap;



//----CONSTRUCTORS----//

	/**
	 * The package-protected default constructor. Only classes which
	 * implement the PolynomialFactory interface should be able to construct
	 * a Polynomial object.
	 */
	TreePolynomial() {
		// Initialize the coefficient map
		coeffToPowerMap = new TreeMap<Integer, Integer>();
	}



//----GETTERS----//

	/**
	 * Returns an integer representing the coefficient of the x^power term.
	 * @param power The power (degree) of the target term.
	 * @return An integer representing the coefficient of the x^power term.
	 */
	public int getCoefficient(final int power) {
		/* Look for a coefficient value mapped to the specified power. If
		 * none exists, the coefficient of that term is zero.
		*/
		return coeffToPowerMap.getOrDefault(power, 0);
	}

	/**
	 * Returns the degree of this Polynomial. This is the greatest power of
	 *  all of its terms.
	 * @return The degree of this Polynomial.
	 */
	public int getDegree() {
		// All coefficients are zero if the map is empty.
		if (coeffToPowerMap.isEmpty()) {
			return 0;
		}

		/* TreeMap's keys are always sorted in ascending order. Because of
		 * this, we are guaranteed that the last value in the key set is the
		 * highest value key. For our purposes, this means that the following
		 * line of code returns the highest power (a.k.a. the degree) of the
		 * Polynomial.
		 *  */
		return coeffToPowerMap.lastKey();
	}

	/**
	 * Returns a List of the term degrees, beginning with the greatest and
	 *  going in order to the smallest.
	 * @return A List of the term degrees.
	 */
	public List<Integer> getPowerList() {
		List<Integer> powerList = new ArrayList<Integer>();

		// Return an empty list if there are no terms. No terms mean no powers.
		if (!coeffToPowerMap.isEmpty()) {
			// Add all keys (powers) from the coefficient map.
			powerList.addAll(coeffToPowerMap.keySet());

			/* TreeMaps are guaranteed to return their map's keySet in the
			 * natural order of their data type, which for Integers is in
			 * ascending (lowest-to-highest) order. Therefore, to meet the
			 * specification of a descending (highest-to-lowest) order, we may
			 * simply reverse the existing order of the list.
			 */
			Collections.reverse(powerList);
		}

		return powerList;
	}



//----SETTERS----//

	/**
	 * Sets the coefficient of the x^power term to the provided coefficient.
	 * @param coefficient The coefficient to set.
	 * @param power The power of the target term.
	 */
	public void setCoefficient(final int coefficient, final int power) {
		/* Incoming coefficients of zero do not need to be stored, as the
		 * value for their associated term will always be zero. This fact is
		 * reflected in the getCoefficient method's implementation.
		*/
		if (coefficient == 0) {
			return;
		}

		// Map the given coefficient to the given power.
		coeffToPowerMap.put(power, coefficient);
	}



//----OPERATORS----//

	/**
	 * Return the result of adding the Polynomial other to this Polynomial.
	 * Does not modify this Polynomial.
	 * @param other The Polynomial to add.
	 * @return The result of adding the Polynomial other to this Polynomial.
	 */
	public Polynomial add(final Polynomial other) {
		// Return the current Polynomial if the other one has no terms.
		if (other.getDegree() == 0) {
			return this;
		}

		Polynomial sum = new TreePolynomial();

		List<Integer> thisOtherPowerListUnion = this.getPowerList();
		List<Integer> otherPowerList = other.getPowerList();

		// Set theory: A u B = A + (B - A)
		otherPowerList.removeAll(thisOtherPowerListUnion);
		thisOtherPowerListUnion.addAll(otherPowerList);

		// Build polynomial from summing coefficients of all powers
		for (Integer power : thisOtherPowerListUnion) {
			int coeffSum = this.getCoefficient(power) + other.getCoefficient(power);

			sum.setCoefficient(coeffSum, power);
		}

		return sum;
	}



//----UTILITIES----//

	/**
	 * Evaluates the polynomial for the value x and returns the result p(x).
	 * Implementations must use Math.pow(double, double) for calculating
	 * exponents.
	 * @param x The value for the polynomial variable x.
	 * @return The evaluation result.
	 */
	public double evaluate(final double x) {
		// Aggregate the individual term values for the total sum.
		double termAggregate = 0;
		for (Integer power : this.getPowerList()) {
			termAggregate += this.getCoefficient(power) * Math.pow(x, power);
		}

		return termAggregate;
	}

	/**
	 * Returns the String representation of the polynomial in canonical form.
	 * For example, 3*x^2 + 2*x + 1 would be returned as
	 * "(3x^2) + (2x) + (1)". In particular the String explicitly contains the
	 * power value for degrees of 2 or greater, simply "x" for degree of 1,
	 * and just the coefficient for degree of 0. Additionally, any term whose
	 * coefficient is 0 should not appear in the String unless the polynomial
	 * has only a single constant term of 0.
	 * @override toString in class Object
	 * @return The String representation of the polynomial.
	 */
	public String toString() {
		// If there are no terms, the Polynomial will always evaluate to zero.
		if (this.getPowerList().isEmpty()) {
			return "(0)";
		}
		/* Canon String representations of the individual terms are stored in
		 * descending order of their powers.
		 */
		List<String> canonTermStringList = new ArrayList<String>();
		String fullCanonicalString = "";

		for (Integer power : this.getPowerList()) {
			if (power == 0) {
				canonTermStringList.add(
						"(" + this.getCoefficient(power) + ")");
			} else if (power == 1) {
				canonTermStringList.add(
						"(" + this.getCoefficient(power) + "x)");
			} else {
				canonTermStringList.add(
						"(" + this.getCoefficient(power) + "x^" + power + ")");
			}
		}

		for (int i = 0; i < canonTermStringList.size(); i++) {
			fullCanonicalString += canonTermStringList.get(i);

			// Add a " + " between terms.
			if (i < canonTermStringList.size() - 1) {
				fullCanonicalString += " + ";
			}
		}

		return fullCanonicalString;
	}
}
