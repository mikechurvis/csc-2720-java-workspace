/**
 * This lab has us build a rudimentary "job simulator". This is a time-based
 * simulator (events handled over a timestep)
 * @author Michael
 */
package edu.gsu.csc2720.lab4.mchurvis;
