package edu.gsu.student.churvis.lab1;

/**
 * The abstract superclass for all MyMath implementing classes.
 * @author Mike Churvis
 *
 */
abstract class MyMath {
	/**
	 * The internal variable down to which the factUntil method counts
	 * when solving its bounded factorial.
	 */
	protected int calcVar;

	/**
	 * The internal constructor for the abstract class. Initializes calcVar.
	 * @param x The value of the instance variable calcVar.
	 */
	MyMath(final int x) {
		calcVar = x;
	}

	/**
	 * Gets the internal calculation variable.
	 * @return the current value of the internal calculation variable.
	 */
	public int getCalcVar() {
		return calcVar;
	}

	/**
	 * Specifies that all inheriting classes must define a factUntil function
	 * taking a single parameter of type int.
	 * @param x the integer that will have its factorial calculated until a
	 * certain point.
	 * @return the partial factorial of the given number.
	 */
	public abstract int factUntil(int x);
}
