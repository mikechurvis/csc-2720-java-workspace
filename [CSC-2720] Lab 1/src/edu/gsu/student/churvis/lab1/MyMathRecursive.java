package edu.gsu.student.churvis.lab1;

/**
 * The MyMathRecursive class implements MyMath to recursively solve a
 * partial factorial problem.
 * @author Mike Churvis
 */
public class MyMathRecursive extends MyMath {

	/**
	 * The constructor for MyMathRecursive.
	 * @param x The value to which the instance variable will be initialized.
	 */
	MyMathRecursive(final int x) {
		super(x);
	}

	/**
	 * Solves the factorial of x until x is either equal to calcVar or 1.
	 * @param x The number to have its factorial solved.
	 * @return The factorial of x bounded by a hidden variable.
	 */
	public int factUntil(final int x) {

		/* The factorials of 1 and 0 are always 1. For convenience, we return
		 * a value of 1 for negative numbers as well, as the factorials of
		 * negative integers are not well defined.
		 */
		if (x <= 1)	{
			return 1;
		}

		/* If the given number is equal to the calculation variable, then no
		 * recursion is required. The factorial ends here.
		 */
		if (x == calcVar) {
			return calcVar;
		}

		/* Accumulate the factorial by multiplying the given number by the result
		 * of this method called given the decrement of the given number. This
		 * stacks recursively until one of the above conditional statements
		 * evaluates to true. The end result is the partial factorial.
		 */
		return x * factUntil(x - 1);
	}
}
