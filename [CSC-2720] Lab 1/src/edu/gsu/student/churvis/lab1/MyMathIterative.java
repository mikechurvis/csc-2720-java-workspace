package edu.gsu.student.churvis.lab1;

/**
 * The MyMathIterative class implements MyMath to iteratively solve a
 * partial factorial problem.
 * @author Mike Churvis
 */
public class MyMathIterative extends MyMath {

	/**
	 * The constructor for MyMathIterative.
	 * @param x The value to which the instance variable will be initialized.
	 */
	MyMathIterative(final int x) {
		super(x);
	}

	/**
	 * Solves the partial factorial of a given integer using iteration.
	 * @param x the integer to have its partial factorial solved.
	 * @return the partial factorial of the given integer.
	 */
	public int factUntil(final int x) {

		/* The factorials of 1 and 0 are always 1. For convenience, we return
		 * a value of 1 for negative numbers as well, as the factorials of
		 * negative integers are not well defined.
		 */
		if (x <= 1)	{
			return 1;
		}

		/* If the given number is equal to the calculation variable, then no
		 * iteration is required. The factorial ends here.
		 */
		if (x == calcVar) {
			return calcVar;
		}

		int factAccum = 1;

		/* Accumulate the factorial until the iterator's value either equals the
		 * instance variable or equals 1.
		 */
		for (int i = x; i > 0; i--) {
			factAccum *= i;
			if (i == calcVar) {
				break;
			}
		}

		return factAccum;
	}

}
