package edu.gsu.student.churvis.lab1;

/**
 * The MyMathTester class displays in the console several test cases of the
 * MyMathIterative and MyMathRecursive classes.
 * @author Mike Churvis
 */
public final class MyMathTester {

	/** The first instance variable required in the specifications.	 */
	private static final int INST_VAR_VALUE_1 = 5;

	/** The second instance variable required in the specifications. */
	private static final int INST_VAR_VALUE_2 = 3;

	/** One of three parameter values for the factUntil method. */
	private static final int PARAM_VALUE_1 = 7;

	/** One of three parameter values for the factUntil method. */
	private static final int PARAM_VALUE_2 = 5;

	/** One of three parameter values for the factUntil method. */
	private static final int PARAM_VALUE_3 = 4;

	/**
	 * Empty private constructor to prevent the instantiation of this
	 * utility class.
	 */
	private MyMathTester() {
	}

	/**
	 * The method used to run code in a command console instance.
	 * @param args The array of command line argument strings. Not used here.
	 */
	public static void main(final String[] args) {
		MyMathRecursive mMR1 = new MyMathRecursive(INST_VAR_VALUE_1);
		MyMathIterative mMI1 = new MyMathIterative(INST_VAR_VALUE_1);
		MyMathRecursive mMR2 = new MyMathRecursive(INST_VAR_VALUE_2);
		MyMathIterative mMI2 = new MyMathIterative(INST_VAR_VALUE_2);

		System.out.println(String.format(
				"Instance Variable: %1$d\n"
				+ "MyMathRecursive.factUntil(%3$d) = %6$d\n"
				+ "MyMathIterative.factUntil(%3$d) = %7$d\n"
				+ "MyMathRecursive.factUntil(%4$d) = %8$d\n"
				+ "MyMathIterative.factUntil(%4$d) = %9$d\n"
				+ "MyMathRecursive.factUntil(%5$d) = %10$d\n"
				+ "MyMathIterative.factUntil(%5$d) = %11$d\n",
				INST_VAR_VALUE_1, INST_VAR_VALUE_2,
				PARAM_VALUE_1, PARAM_VALUE_2, PARAM_VALUE_3,
				mMR1.factUntil(PARAM_VALUE_1), mMI1.factUntil(PARAM_VALUE_1),
				mMR1.factUntil(PARAM_VALUE_2), mMI1.factUntil(PARAM_VALUE_2),
				mMR1.factUntil(PARAM_VALUE_3), mMI1.factUntil(PARAM_VALUE_3)));

		System.out.println(String.format(
				"Instance Variable: %2$d\n"
				+ "MyMathRecursive.factUntil(%3$d) = %6$d\n"
				+ "MyMathIterative.factUntil(%3$d) = %7$d\n"
				+ "MyMathRecursive.factUntil(%4$d) = %8$d\n"
				+ "MyMathIterative.factUntil(%4$d) = %9$d\n"
				+ "MyMathRecursive.factUntil(%5$d) = %10$d\n"
				+ "MyMathIterative.factUntil(%5$d) = %11$d\n",
				INST_VAR_VALUE_1, INST_VAR_VALUE_2,
				PARAM_VALUE_1, PARAM_VALUE_2, PARAM_VALUE_3,
				mMR2.factUntil(PARAM_VALUE_1), mMI2.factUntil(PARAM_VALUE_1),
				mMR2.factUntil(PARAM_VALUE_2), mMI2.factUntil(PARAM_VALUE_2),
				mMR2.factUntil(PARAM_VALUE_3), mMI2.factUntil(PARAM_VALUE_3)));
	}
}
